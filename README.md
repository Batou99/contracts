# Volders Contract Manager

## Installation

To avoid having to install programs locally the project is bundled with a `docker-compose.yml` file

To install just do

```
docker image build -t volders_phoenix_dev -f ops/docker/Dockerfile.phoenix_dev ops/docker
docker-compose build
./scripts/up
```

once docker containers are up and running do

```
./scripts/mix ecto.setup
./scripts/mix phx.server
```

To stop the whole stack do `./scripts/down`

To fiddle with the application (run a console, etc) you can `./scripts/console website`
To access the postgres DB server you can `./scripts/console postgres`

## Deployment

![Deployment diagram](https://bitbucket.org/repo/ypexzeR/images/1013823250-Captura%20de%20pantalla%202018-05-29%20a%20las%2010.55.41.png)

The application is already deployed on [digital ocean](http://volders.9thingstodo.com)
Deployment has been done using terraform and distillery.

See `ops/terraform` folder and `/volders/Dockerfile`

## Seed data

There is one user

* email: testuser@example.com
* password: somesecret

It has already created 6 contracts but only 5 show on the listing page because one of them has already ended.

## Troubleshooting

If you want to run locally (without docker)

* Check `volders/config/dev.exs`: DB `hostname` is pointing to `postgres`. Either add and `/etc/hosts` entry pointing to your postgres instance or change the value here.

* If you have tried the `docker` version and want to run locally afterwards, ensure you recompile the dependencies, `bcrypt` library is compiled for the running system (linux under docker) so if you want to run locally on a mac it won't work without recompiling first. (`mix deps.compile`)

* The scripts under the `scripts` folder are meant to be used only on the docker version, to run locally just use the normal mix commands (`mix ecto.setup`, `mix phx.server`)

