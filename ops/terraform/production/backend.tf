# Set the variable value in *.tfvars file
# or using -var="do_token=..." CLI option
variable "do_token" {}
variable "version" {}
variable "db_user" {}
variable "db_password" {}
variable "docker_password" {}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.do_token}"
}

data "template_file" "docker-compose" {
  template = "${file("./docker-compose.tpl.yml")}"
  vars {
    servername       = "production"
    version          = "${var.version}"
    db_user          = "${var.db_user}"
    db_password      = "${var.db_password}"
  }
}

data "template_file" "backend" {
  template = "${file("backend.tpl.yml")}"
}

# Create a new Web Droplet in the lon1 region
resource "digitalocean_droplet" "volders" {
  name       = "volders"
  image      = "docker-16-04"
  region     = "lon1"
  size       = "s-1vcpu-1gb"
  ssh_keys   = ["b4:f8:d0:59:98:a7:ec:ae:8e:c0:b5:53:34:42:d7:6c"]
  volume_ids = ["${digitalocean_volume.dbdisk-production.id}"]
  user_data  = "${data.template_file.backend.rendered}"

  provisioner "file" {
    content     = "${data.template_file.docker-compose.rendered}"
    destination = "/mnt/docker-compose.yml"
  }
}

resource "digitalocean_volume" "dbdisk-production" {
  name        = "dbdisk-volders"
  region      = "lon1"
  size        = "2"
  description = "Disk for app DB"
}

# Create a new domain
resource "digitalocean_domain" "production" {
  name       = "volders.9thingstodo.com"
  ip_address = "${digitalocean_droplet.volders.ipv4_address}"
}

# This just works on MacOS
resource "null_resource" "flush_cache" {
  provisioner "local-exec" {
    command = <<EOF
      sudo dscacheutil -flushcache
      sudo killall -HUP mDNSResponder;
    EOF
  }
}

output "ip" {
  value = "${digitalocean_droplet.volders.ipv4_address}"
}
