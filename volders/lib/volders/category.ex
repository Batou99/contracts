defmodule Volders.Category do
  use Ecto.Schema
  import Ecto.Changeset

  alias Volders.Repo

  schema "categories" do
    field :name, :string

    many_to_many :vendors, Volders.Vendor, join_through: Volders.VendorCategory

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> put_assoc(:vendors, get_vendors(attrs[:vendors]))
  end

  def get_vendors(vendors_params) do
    vendors_params || []
    |> Enum.map(&Repo.get(Vendor, &1))
  end
end
