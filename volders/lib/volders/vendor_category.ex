defmodule Volders.VendorCategory do
  use Ecto.Schema
  import Ecto.Changeset


  schema "vendor_categories" do
    belongs_to :vendor,   Volders.Vendor
    belongs_to :category, Volders.Category

    timestamps()
  end

  @doc false
  def changeset(vendor_category, attrs) do
    vendor_category
    |> cast(attrs, [])
    |> validate_required([])
  end
end
