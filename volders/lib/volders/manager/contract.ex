defmodule Volders.Manager.Contract do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Volders.{Vendor, Category, Repo}
  alias Volders.Coherence.User

  schema "contracts" do
    field :costs,   :float
    field :ends_on, :date

    belongs_to :user,     User
    belongs_to :vendor,   Vendor
    belongs_to :category, Category

    timestamps()
  end

  @doc false
  def changeset(contract, attrs) do
    contract
    |> cast(attrs, [:vendor_id, :category_id, :user_id, :costs, :ends_on])
    |> validate_required([:vendor_id, :category_id, :user_id, :costs, :ends_on])
    |> validate_number(:costs, greater_than: 0)
    |> validate_vendor_and_category
  end

  def by_user(query, user) do
    from c in query,
    where: c.user_id == ^user.id
  end

  def ends_on_today_or_future(query) do
    from c in query,
    where: c.ends_on >= ^Ecto.Date.utc
  end

  defp validate_vendor_and_category(changeset) do
    category_id = get_field(changeset, :category_id)
    vendor_id   = get_field(changeset, :vendor_id)

    if category_id && vendor_id do
      category    = Repo.get(Category, category_id)
      vendor      = Repo.get(Vendor, vendor_id) |> Repo.preload(:categories)

      if !Enum.member?(vendor.categories, category) do
        changeset
        |> add_error(:category_id, "Must be provided by current vendor")
      else
        changeset
      end
    else
      changeset
    end
  end
end
