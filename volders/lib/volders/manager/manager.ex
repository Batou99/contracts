defmodule Volders.Manager do
  @moduledoc """
  The Manager context.
  """

  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias Volders.Repo
  alias Volders.Manager.Contract
  alias Volders.Coherence.User

  @doc """
  Returns the list of contracts.

  ## Examples

      iex> list_contracts(user)
      [%Contract{user_id: user.id, ends_on: future_date}, ...]

  """
  def list_contracts(user = %User{}) do
    Contract
    |> Contract.by_user(user)
    |> Contract.ends_on_today_or_future
    |> order_by(asc: :ends_on)
    |> Repo.all
    |> Repo.preload([:vendor, :category])
  end

  @doc """
  Gets a single contract.

  Raises `Ecto.NoResultsError` if the Contract does not exist.

  ## Examples

      iex> get_contract!(user, 123)
      %Contract{user_id: user.id, ends_on: future_date}

      iex> get_contract!(user2, 123)
      ** (Ecto.NoResultsError)

  """
  def get_contract!(user = %User{}, id) do
    Contract
    |> Contract.by_user(user)
    |> Contract.ends_on_today_or_future
    |> Repo.get!(id)
    |> Repo.preload([:vendor, :category])
  end

  @doc """
  Creates a contract.

  ## Examples

      iex> create_contract(user, %{field: value})
      {:ok, %Contract{user_id: user.id}}

      iex> create_contract(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_contract(%User{id: id}, attrs \\ %{}) do
    attrs_with_user = attrs |> AtomicMap.convert |> Map.merge(%{user_id: id})

    %Contract{}
    |> Contract.changeset(attrs_with_user)
    |> Repo.insert()
  end

  @doc """
  Creates a contract but only if ends_on date is today or in the future.

  ## Examples

      iex> create_future_contract(user, %{field: value})
      {:ok, %Contract{user_id: user.id}}

      iex> create_future_contract(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_future_contract(%User{id: id}, attrs \\ {}) do
    attrs_with_user = attrs |> AtomicMap.convert |> Map.merge(%{user_id: id})

    %Contract{}
    |> Contract.changeset(attrs_with_user)
    |> validate_ends_on_today_or_future
    |> Repo.insert()
  end

  @doc """
  Updates a contract.
  Cannot update user.

  ## Examples

      iex> update_contract(contract, %{field: new_value})
      {:ok, %Contract{}}

      iex> update_contract(contract, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_contract(%Contract{} = contract, attrs) do
    contract
    |> Contract.changeset(Map.delete(attrs, :user_id))
    |> Repo.update()
  end

  @doc """
  Updates a contract but only if ends_on date is today or a future date.
  Cannot update user.

  ## Examples

      iex> update_future_contract(contract, %{field: new_value})
      {:ok, %Contract{}}

      iex> update_future_contract(contract, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_future_contract(%Contract{} = contract, attrs) do
    contract
    |> Contract.changeset(Map.delete(attrs, :user_id))
    |> validate_ends_on_today_or_future
    |> Repo.update()
  end

  @doc """
  Deletes a Contract.

  ## Examples

      iex> delete_contract(contract)
      {:ok, %Contract{}}

      iex> delete_contract(contract)
      {:error, %Ecto.Changeset{}}

  """
  def delete_contract!(%User{id: id}, %Contract{} = contract) do
    if contract.user_id == id do
      Repo.delete contract
    else
      Repo.delete! %Contract{id: 0}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking contract changes with the user already set.

  ## Examples

      iex> change_contract(user, contract)
      %Ecto.Changeset{source: %Contract{}}

  """
  def change_contract(%User{id: id}, %Contract{} = contract) do
    Contract.changeset(contract, %{user_id: id})
  end

  # NOTE: We add this here because we want to be able to create Contracts that end in the past through
  # the console or seeds but not through the Web interface. So this way we can either call Manager.update_contract/2
  # or Manager.update_future_contract/2 (or its create versions) depending on what we want
  defp validate_ends_on_today_or_future(changeset) do
    ends_on = get_field(changeset, :ends_on)

    if ends_on && Date.compare(ends_on, Date.utc_today) == :lt do
      changeset
      |> add_error(:ends_on, "Must be from today onwards")
    else
      changeset
    end
  end
end
