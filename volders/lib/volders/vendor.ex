defmodule Volders.Vendor do
  use Ecto.Schema
  import Ecto.Changeset

  alias Volders.Repo

  schema "vendors" do
    field :name, :string

    many_to_many :categories, Volders.Category, join_through: Volders.VendorCategory

    timestamps()
  end

  @doc false
  def changeset(vendor, attrs) do
    vendor
    |> Repo.preload(:categories)
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> put_assoc(:categories, get_categories(attrs[:categories]))
  end

  def get_categories(categories_params) do
    categories_params || []
    |> Enum.map(&Repo.get(Category, &1))
  end
end
