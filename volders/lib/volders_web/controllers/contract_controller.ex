defmodule VoldersWeb.ContractController do
  use VoldersWeb, :controller

  alias Volders.{Manager, Vendor, Category, Repo}
  alias Volders.Manager.Contract

  import Coherence, only: [current_user: 1]

  def index(conn, _params) do
    contracts = current_user(conn)
                |> Manager.list_contracts
    render(conn, "index.html", contracts: contracts)
  end

  def new(conn, _params) do
    changeset  = Manager.change_contract(current_user(conn), %Contract{})
    vendors    = Vendor   |> Repo.all
    categories = Category |> Repo.all

    render(conn, "new.html", changeset: changeset, vendors: vendors, categories: categories)
  end

  def create(conn, %{"contract" => contract_params}) do
    vendors    = Vendor   |> Repo.all
    categories = Category |> Repo.all
    case Manager.create_future_contract(current_user(conn), contract_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Contract created successfully.")
        |> redirect(to: contract_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, vendors: vendors, categories: categories)
    end
  end

  def show(conn, %{"id" => id}) do
    contract = Manager.get_contract!(current_user(conn), id)
    render(conn, "show.html", contract: contract)
  end

  def edit(conn, %{"id" => id}) do
    contract   = Manager.get_contract!(current_user(conn), id)
    changeset  = Manager.change_contract(current_user(conn), contract)
    vendors    = Vendor   |> Repo.all
    categories = Category |> Repo.all

    render(conn, "edit.html", contract: contract, changeset: changeset, vendors: vendors, categories: categories)
  end

  def update(conn, %{"id" => id, "contract" => contract_params}) do
    contract   = Manager.get_contract!(current_user(conn), id)
    vendors    = Vendor |> Repo.all
    categories = Category |> Repo.all

    case Manager.update_future_contract(contract, contract_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Contract updated successfully.")
        |> redirect(to: contract_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", contract: contract, changeset: changeset, vendors: vendors, categories: categories)
    end
  end

  def delete(conn, %{"id" => id}) do
    contract = Manager.get_contract!(current_user(conn), id)
    {:ok, _contract} = Manager.delete_contract!(current_user(conn), contract)

    conn
    |> put_flash(:info, "Contract deleted successfully.")
    |> redirect(to: contract_path(conn, :index))
  end
end
