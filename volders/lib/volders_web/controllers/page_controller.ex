defmodule VoldersWeb.PageController do
  use VoldersWeb, :controller

  def index(conn, _params) do
    if Coherence.logged_in?(conn) do
      conn |> redirect(to: "/contracts")
    else
      conn |> redirect(to: "/login")
    end
  end
end
