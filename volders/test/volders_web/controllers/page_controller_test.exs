defmodule VoldersWeb.PageControllerTest do
  use VoldersWeb.ConnCase

  alias Volders.Coherence.User
  alias Volders.Repo

  test "on :index redirect to /contracts if logged in", %{conn: conn} do
    john = %User{name: "John Doe"} |> Repo.insert!
    conn = assign(conn, :current_user, john) |> get("/")

    assert redirected_to(conn, 302) =~ "/contracts"
  end

  test "on :index redirect to /login if not logged in", %{conn: conn} do
    conn = get conn, "/"
    assert redirected_to(conn, 302) =~ "/login"
  end
end
