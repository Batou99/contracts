defmodule VoldersWeb.ContractControllerTest do
  use VoldersWeb.ConnCase
  import Ecto.Query

  alias Volders.Manager.Contract
  alias Volders.{Manager, Repo, Category, Vendor}
  alias Volders.Coherence.User

  setup %{conn: conn} do
    john     = %User{name: "John Doe"}                      |> Repo.insert!
    jane     = %User{name: "Jane Noone"}                    |> Repo.insert!
    internet = %Category{name: "Internet"}                  |> Repo.insert!
    vendor   = %Vendor{name: "foo", categories: [internet]} |> Repo.insert!
    today    = Date.utc_today()
    tomorrow = Date.add(today, 1)

    valid_attrs = %{
      costs:       120.5,
      ends_on:     today,
      vendor_id:   vendor.id,
      user_id:     john.id,
      category_id: internet.id
    }
    update_attrs  = %{
      costs:   456.7,
      ends_on: tomorrow
    }
    invalid_attrs = %{
      costs:       nil,
      ends_on:     nil,
      vendor_id:   vendor.id,
      user_id:     john.id,
      category_id: internet.id
    }

    {:ok, contract} = john |> Manager.create_contract(valid_attrs)

    {:ok,
      conn:          assign(conn, :current_user, john),
      valid_attrs:   valid_attrs,
      update_attrs:  update_attrs,
      invalid_attrs: invalid_attrs,
      contract:      contract,
      john:          john,
      jane:          jane,
      today:         today,
      tomorrow:      tomorrow
    }
  end

  describe "index" do
    test "lists all contracts", %{conn: conn} do
      conn = get conn, contract_path(conn, :index)
      assert html_response(conn, 200) =~ "My Contracts"
    end
  end

  describe "show" do
    test "shows a contract if it exists", %{conn: conn, contract: contract} do
      conn = get conn, contract_path(conn, :show, contract)
      assert html_response(conn, 200) =~ "Contract Details"
    end

    test "returns 404 if contract does not exits", %{conn: conn} do
      assert_error_sent 404, fn ->
        conn = get conn, contract_path(conn, :show, %Contract{id: 0})
      end
    end
  end

  describe "new contract" do
    test "renders form", %{conn: conn} do
      conn = get conn, contract_path(conn, :new)
      assert html_response(conn, 200) =~ "New Contract"
    end
  end

  describe "create contract" do
    test "redirects to index when data is valid", %{conn: conn, valid_attrs: valid_attrs} do
      post_conn = post conn, contract_path(conn, :create), contract: valid_attrs

      assert redirected_to(post_conn) == contract_path(conn, :index)

      contract = from(c in Contract, limit: 1, order_by: [desc: c.inserted_at]) |> Repo.one
      conn     = get conn, contract_path(conn, :show, contract)

      assert html_response(conn, 200) =~ "Contract Details"
    end

    test "renders errors when data is invalid", %{conn: conn, invalid_attrs: invalid_attrs} do
      conn = post conn, contract_path(conn, :create), contract: invalid_attrs
      assert html_response(conn, 200) =~ "New Contract"
    end

    test "does not allow to create contracts with a past date", %{conn: conn, valid_attrs: valid_attrs, today: today} do
      yesterday = Date.add(today, -1)
      conn = post conn, contract_path(conn, :create), contract: %{valid_attrs | ends_on: yesterday}
      assert html_response(conn, 200) =~ "New Contract"
    end
  end

  describe "edit contract" do
    test "renders form for editing chosen contract", %{conn: conn, contract: contract} do
      conn = get conn, contract_path(conn, :edit, contract)
      assert html_response(conn, 200) =~ "Edit Contract"
    end
  end

  describe "update contract" do
    test "redirects to index when data is valid", %{conn: conn, contract: contract, update_attrs: update_attrs} do
      put_conn = put conn, contract_path(conn, :update, contract), contract: update_attrs
      assert redirected_to(put_conn) == contract_path(conn, :index)

      conn = get conn, contract_path(conn, :show, contract)

      assert html_response(conn, 200) =~ "Contract Details"
    end

    test "renders errors when data is invalid", %{conn: conn, contract: contract, invalid_attrs: invalid_attrs} do
      conn = put conn, contract_path(conn, :update, contract), contract: invalid_attrs
      assert html_response(conn, 200) =~ "Edit Contract"
    end

    test "does not allow to update contracts with a past date", %{
      conn:         conn,
      contract:     contract,
      update_attrs: update_attrs,
      today:        today
    } do
      yesterday = Date.add(today, -1)

      conn = put conn, contract_path(conn, :update, contract), contract: %{update_attrs| ends_on: yesterday}
      assert html_response(conn, 200) =~ "Edit Contract"
    end
  end

  describe "delete contract" do

    test "deletes chosen contract", %{conn: conn, contract: contract} do
      delete_conn = delete conn, contract_path(conn, :delete, contract)
      assert redirected_to(delete_conn) == contract_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, contract_path(conn, :show, contract)
      end
    end
  end
end
