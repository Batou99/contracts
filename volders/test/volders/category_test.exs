defmodule Volders.CategoryTest do
  use Volders.DataCase

  alias Volders.{Category, Vendor}
  alias Volders.Repo

  setup do
    foo   = Vendor.changeset(%Vendor{}, %{name: "foo"}) |> Repo.insert!
    bar   = Vendor.changeset(%Vendor{}, %{name: "bar"}) |> Repo.insert!
    internet = Category.changeset(%Category{}, %{name: "Internet"}) |> Repo.insert!
    {:ok, foo: foo, bar: bar, internet: internet}
  end

  test "changeset with valid attributes: many to many", context do
    foo      = context[:foo]
    bar      = context[:bar]
    internet = context[:internet]

    category = Category.changeset(internet, %{vendors: [foo, bar]}) |> Repo.update!

    assert category.vendors == [foo, bar]
  end

  test "changeset with valid attributes: only base data", context do
    category = Category.changeset(context[:internet], %{name: "low speed internet"}) |> Repo.update!

    assert category.name == "low speed internet"
  end
end
