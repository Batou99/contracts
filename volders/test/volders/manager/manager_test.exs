defmodule Volders.ManagerTest do
  use Volders.DataCase

  alias Volders.Manager
  alias Volders.{Repo, Category, Vendor}
  alias Volders.Coherence.User

  describe "contracts" do
    alias Volders.Manager.Contract

    setup do
      john     = %User{name: "John Doe"}                      |> Repo.insert!
      jane     = %User{name: "Jane Noone"}                    |> Repo.insert!
      internet = %Category{name: "Internet"}                  |> Repo.insert!
      vendor   = %Vendor{name: "foo", categories: [internet]} |> Repo.insert!
      today    = Date.utc_today()
      tomorrow = Date.add(today, 1)

      valid_attrs = %{
        costs:       120.5,
        ends_on:     today,
        vendor_id:   vendor.id,
        user_id:     john.id,
        category_id: internet.id
      }
      update_attrs  = %{
        costs:   456.7,
        ends_on: tomorrow
      }
      invalid_attrs = %{
        costs:       nil,
        ends_on:     nil,
        vendor_id:   vendor.id,
        user_id:     john.id,
        category_id: internet.id
      }

      contract = john
                 |> Manager.create_contract(valid_attrs)
                 |> elem(1)
                 |> Repo.preload([:vendor, :category])

      {:ok,
        valid_attrs:   valid_attrs,
        update_attrs:  update_attrs,
        invalid_attrs: invalid_attrs,
        contract:      contract,
        john:          john,
        jane:          jane,
        today:         today,
        tomorrow:      tomorrow
      }
    end

    test "list_contracts/0 returns all contracts", %{contract: contract, john: john, jane: jane} do
      assert Manager.list_contracts(john) == [contract]
      assert Manager.list_contracts(jane) == []
    end

    test "get_contract!/1 returns the contract with given id", %{contract: contract, john: john, jane: jane} do
      assert       Manager.get_contract!(john, contract.id) == contract
      assert_raise Ecto.NoResultsError, fn -> Manager.get_contract!(jane, contract.id) end
    end

    test "create_contract/1 with valid data creates a contract", %{jane: jane, today: today, valid_attrs: valid_attrs} do
      assert {:ok, %Contract{} = contract} = Manager.create_contract(jane, valid_attrs)

      assert contract.costs == 120.5
      assert contract.ends_on == today
      assert contract.user_id == jane.id
    end

    test "create_contract/1 with invalid data returns error changeset", %{jane: jane, invalid_attrs: invalid_attrs} do
      assert {:error, %Ecto.Changeset{}} = Manager.create_contract(jane, invalid_attrs)
    end

    test "create_future_contract only creates contracts if date is from today onwards", %{
      jane:        jane,
      today:       today,
      valid_attrs: valid_attrs
    } do
      yesterday = Date.add(today, -1)

      assert {:error, _contract}           = Manager.create_future_contract(jane, %{valid_attrs | ends_on: yesterday})
      assert {:ok, %Contract{} = contract} = Manager.create_future_contract(jane, valid_attrs)

      assert contract.costs == 120.5
      assert contract.ends_on == today
      assert contract.user_id == jane.id
    end

    test "update_contract/2 with valid data updates the contract", %{
      contract:     contract,
      update_attrs: update_attrs,
      jane:         jane,
      john:         john,
      tomorrow:     tomorrow
    } do
      assert {:ok, contract} = Manager.update_contract(contract, update_attrs)
      assert %Contract{} = contract
      assert contract.costs == 456.7
      assert contract.ends_on == tomorrow

      # NOTE: But it does not update the user_id
      change_user_attrs = Map.merge(update_attrs, %{user_id: jane.id})
      assert {:ok, contract} = Manager.update_contract(contract, change_user_attrs)
      assert contract.user_id == john.id
    end

    test "update_contract/2 with invalid data returns error changeset", %{
      contract:      contract,
      invalid_attrs: invalid_attrs,
      john:          john
    } do
      assert {:error, %Ecto.Changeset{}} = Manager.update_contract(contract, invalid_attrs)
      assert contract == Manager.get_contract!(john, contract.id)
    end

    test "update_future_contract only updates contracts if date is from today onwards", %{
      contract:     contract,
      update_attrs: update_attrs,
      tomorrow:     tomorrow
    } do
      yesterday = Date.add(tomorrow, -2)

      assert {:error, %Ecto.Changeset{}} = Manager.update_future_contract(contract, %{update_attrs | ends_on: yesterday})
      assert {:ok, contract}             = Manager.update_future_contract(contract, update_attrs)

      assert %Contract{}      = contract
      assert contract.costs   == 456.7
      assert contract.ends_on == tomorrow
    end

    test "delete_contract/1 deletes the contract", %{contract: contract, john: john} do
      assert {:ok, %Contract{}} = Manager.delete_contract!(john, contract)
      assert_raise Ecto.NoResultsError, fn -> Manager.get_contract!(john, contract.id) end
    end

    test "delete_contract/1 does not delete the contract if does not belong to the user",
    %{contract: contract, jane: jane} do
      assert_raise Ecto.StaleEntryError, fn -> Manager.delete_contract!(jane, contract) end
    end

    test "change_contract/1 returns a contract changeset with the user already set", %{john: john} do
      id = john.id

      assert %{user_id: id} == Manager.change_contract(john, %Contract{}).changes
    end
  end
end
