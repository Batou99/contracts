defmodule Volders.Manager.ContractTest do
  use Volders.DataCase

  alias Volders.Manager.Contract
  alias Volders.{Category, Vendor, Repo}
  alias Volders.Coherence.User

  setup do
    user     = %User{name: "John Doe"}                      |> Repo.insert!
    internet = %Category{name: "Internet"}                  |> Repo.insert!
    dsl      = %Category{name: "DSL"}                       |> Repo.insert!
    foo      = %Vendor{name: "foo", categories: [internet]} |> Repo.insert!

    {:ok, foo: foo, internet: internet, dsl: dsl, user: user}
  end

  test "changeset with valid attributes", %{foo: foo, internet: internet, user: user} do
    attrs    = %{
      costs:       100.1,
      ends_on:     Date.utc_today(),
      vendor_id:   foo.id,
      category_id: internet.id,
      user_id:     user.id
    }
    contract = Contract.changeset(%Contract{}, attrs)

    assert contract.valid?
  end

  test "validate vendor and category", %{foo: foo, dsl: dsl, user: user} do
    attrs = %{
      costs:       100.1,
      ends_on:     Date.utc_today(),
      vendor_id:   foo.id,
      category_id: dsl.id,
      user_id:     user.id
    }
    contract = Contract.changeset(%Contract{}, attrs)

    refute contract.valid?
    assert %{category_id: ["Must be provided by current vendor"]} = errors_on(contract)
  end

  test "by_user filter only shows contracts belonging to a user", %{user: john, foo: foo, internet: internet} do
    jane     = %User{name: "Jane Noone"} |> Repo.insert!
    attrs    = %{
      costs:       100.1,
      ends_on:     Date.utc_today(),
      vendor_id:   foo.id,
      category_id: internet.id,
      user_id:     john.id
    }
    contract = Contract.changeset(%Contract{}, attrs) |> Repo.insert!

    assert Contract |> Contract.by_user(john) |> Repo.all == [contract]
    assert Contract |> Contract.by_user(jane) |> Repo.all == []
  end

  test "ends_on_today_or_future only shows contracts ending later than today", %{
    user:     john,
    foo:      foo,
    internet: internet
  } do
    today     = Date.utc_today()
    yesterday = Date.add(today, -1)
    attrs     = %{
      costs:       100.1,
      ends_on:     yesterday,
      vendor_id:   foo.id,
      category_id: internet.id,
      user_id:     john.id
    }

    Contract.changeset(%Contract{}, attrs)                                    |> Repo.insert!
    new_contract = Contract.changeset(%Contract{}, %{attrs | ends_on: today}) |> Repo.insert!

    assert Contract |> Contract.ends_on_today_or_future |> Repo.all == [new_contract]
  end
end
