defmodule Volders.Coherence.UserTest do
  use Volders.DataCase

  alias Volders.Coherence.User

  @valid_attrs   %{name: "John",  email: "john.doe@gmail.com", password: "12345678"}
  @invalid_attrs %{name: "Peter", email: "notamail",           password: "as"}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?

    assert %{email:    ["has invalid format"]}                = errors_on(changeset)
    assert %{password: ["should be at least 8 character(s)"]} = errors_on(changeset)
  end
end
