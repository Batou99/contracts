defmodule Volders.VendorTest do
  use Volders.DataCase

  alias Volders.{Category, Vendor}
  alias Volders.Repo

  setup do
    foo      = Vendor.changeset(%Vendor{}, %{name: "foo"}) |> Repo.insert!
    internet = Category.changeset(%Category{}, %{name: "Internet"}) |> Repo.insert!
    dsl      = Category.changeset(%Category{}, %{name: "DSL"}) |> Repo.insert!
    {:ok, internet: internet, dsl: dsl, foo: foo}
  end

  test "changeset with valid attributes: many to many", context do
    foo      = context[:foo]
    internet = context[:internet]
    dsl      = context[:dsl]

    vendor   = Vendor.changeset(foo, %{categories: [internet, dsl]}) |> Repo.update!

    assert vendor.categories == [internet, dsl]
  end

  test "changeset with valid attributes: only base data", context do
    vendor = Vendor.changeset(context[:foo], %{name: "bar"}) |> Repo.update!

    assert vendor.name == "bar"
  end
end
