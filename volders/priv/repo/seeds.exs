# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Volders.Repo.insert!(%Volders.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Volders.Repo
alias Volders.Coherence.User
alias Volders.{Manager, VendorCategory, Vendor, Category}
alias Volders.Manager.Contract

Repo.delete_all Contract
Repo.delete_all User
Repo.delete_all VendorCategory
Repo.delete_all Vendor
Repo.delete_all Category

{:ok, test_user} = User.changeset(%User{}, %{
  name: "Test User",
  email: "testuser@example.com",
  password: "somesecret",
  password_confirmation: "somesecret"
}) |> Repo.insert

internet     = %Category{name: "Internet"}     |> Repo.insert!
dsl          = %Category{name: "DSL"}          |> Repo.insert!
phone        = %Category{name: "Phone"}        |> Repo.insert!
mobile_phone = %Category{name: "Mobile Phone"} |> Repo.insert!
electricity  = %Category{name: "Electricity"}  |> Repo.insert!
gas          = %Category{name: "Gas"}          |> Repo.insert!
gym          = %Category{name: "Gym"}          |> Repo.insert!
paid_tv      = %Category{name: "Paid TV"}      |> Repo.insert!

vodaphone  = %Vendor{name: "Vodafone",   categories: [internet, dsl, phone, mobile_phone]} |> Repo.insert!
o2         = %Vendor{name: "O2",         categories: [internet, dsl]}                      |> Repo.insert!
vattenfall = %Vendor{name: "Vattenfall", categories: [electricity, gas]}                   |> Repo.insert!
macfit     = %Vendor{name: "McFit",      categories: [gym]}                                |> Repo.insert!
sky        = %Vendor{name: "Sky",        categories: [paid_tv]}                            |> Repo.insert!

Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, 10),
  costs:       100,
  vendor_id:   vodaphone.id,
  category_id: phone.id
})
Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, 10),
  costs:       120,
  vendor_id:   vodaphone.id,
  category_id: mobile_phone.id
})
Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, 100),
  costs:       200,
  vendor_id:   macfit.id,
  category_id: gym.id
})
Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, 200),
  costs:       1000,
  vendor_id:   vattenfall.id,
  category_id: gas.id
})
Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, 200),
  costs:       1000,
  vendor_id:   vattenfall.id,
  category_id: electricity.id
})
Manager.create_contract(test_user, %{
  ends_on:     Date.add(Date.utc_today, -100),
  costs:       1000,
  vendor_id:   vattenfall.id,
  category_id: electricity.id
})
