defmodule Volders.Repo.Migrations.CreateVendorCategories do
  use Ecto.Migration

  def change do
    create table(:vendor_categories) do
      add :vendor_id, references(:vendors, on_delete: :nothing)
      add :category_id, references(:categories, on_delete: :nothing)

      timestamps()
    end

    create index(:vendor_categories, [:vendor_id])
    create index(:vendor_categories, [:category_id])
  end
end
