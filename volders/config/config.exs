# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :volders,
  ecto_repos: [Volders.Repo]

# Configures the endpoint
config :volders, VoldersWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yd1Lj1nonYPkPYeUJQbY6z6EltDchJTWburU7x25TKPK5IFB3LN0dG0qft3hOS5E",
  render_errors: [view: VoldersWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Volders.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  layout: {VoldersWeb.LayoutView, :app},
  logged_out_url: "/",
  messages_backend: VoldersWeb.Coherence.Messages,
  minimum_password_length: 8,
  module: Volders,
  opts: [:registerable, :authenticatable],
  repo: Volders.Repo,
  router: VoldersWeb.Router,
  user_schema: Volders.Coherence.User,
  web_module: VoldersWeb,
  allow_unconfirmed_access_for: 10000
# %% End Coherence Configuration %%
